import sys
sys.path.append('../')
import gemini
import numpy as np

def bf_search(points, q, n_neighbors):

    plist = list(points)
    bfdists = {p.name : gemini.l2(p,q) for p in plist}
    plist.sort(key=lambda p: gemini.l2(p,q))
    return [(bfdists[p.name], p) for p in plist][:n_neighbors+1]

def testSameNeighbors():
    print("Running test")
    data = np.random.normal(loc=0.0, scale=1.0, size=(10000, 15))
    n_neighbors = 10000
    query = data[0]

    points = [gemini.NDPoint(x,i) for i, x in  enumerate(data)]

    print('building vp tree ...')
    tree = gemini.VPTree(points)
    q = gemini.NDPoint(query)
    vp_neighbors = tree.get_nearest_neighbors(q, n_neighbors)
    bf_neighbors = bf_search(points, q, n_neighbors)
    ##print(vp_neighbors)
    ##print(bf_neighbors)
    print(len(vp_neighbors))
    print(len(bf_neighbors))

    for i in range(n_neighbors):
        ##Either same order, or an effective tie
        if (vp_neighbors[i][1].name != bf_neighbors[i][1].name):
            print("i:" + str(i) + "\t" + str(vp_neighbors[i][1].name) + "\t" + str(vp_neighbors[i][0]) + "\t" + str(bf_neighbors[i][0]) + "\t" + str(bf_neighbors[i][1].name))
        assert ((vp_neighbors[i][1].name == bf_neighbors[i][1].name) )  
    

