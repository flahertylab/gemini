# -*- coding: cp1252 -*-

""" VP-tree implementation authored by "Huy Nguyen" on March 26, 2014. 
    
    Released via Github under the URL: https://github.com/huyng/algorithms/tree/master/vptree

    Modified by "Hachem Saddiki" on September 22, 2015 for faster execution.

    This work is licensed under a Creative Commons Attribution 4.0 International License.
"""

from collections import namedtuple
from collections import deque
import random
import numpy as np
import pickle
import h5py
import logging
import time
from sklearn.decomposition import PCA
import heapq
#from bruteforce2 import revised_nn_euclidean


class NDPoint(object):
    """
    A point in n-dimensional space
    """
    def __init__(self, x, name=None):
        self.x = np.array(x)
        self.name = name
    def __repr__(self):
        return "NDPoint(name=%s, x=%s)" % (self.name, self.x)

class VPTree(object):
    """
    An efficient data structure to perform nearest-neighbor
    search. 
    """
    def __init__(self, points, dist_fn=None):
        self.left = None
        self.right = None
        self.mu = None
        self.dist_fn = dist_fn if dist_fn is not None else l2
        
        ##Don't destroy the old points (lists are mutable)
        points = list(points)

        # choose a better vantage point selection process
        self.vp = points.pop(random.randrange(len(points)))

        if len(points) < 1:
            return

        # choose division boundary at median of distances
        distances = [self.dist_fn(self.vp, p) for p in points]
               
        #self.mu = np.median(distances)
        self.mu = quick_median(distances) #faster execution than np.median()

        left_points = []  # all points inside mu radius
        right_points = []  # all points outside mu radius
        for i, p in enumerate(points):
            d = distances[i]
            if d >= self.mu:
                right_points.append(p)
            else:
                left_points.append(p)

        if len(left_points) > 0:
            self.left = VPTree(points=left_points, dist_fn=self.dist_fn)

        if len(right_points) > 0:
            self.right = VPTree(points=right_points, dist_fn=self.dist_fn)


    def is_leaf(self):
        return (self.left is None) and (self.right is None)


    ### Operations
    def get_nearest_neighbors(self, q, k=1):
        """
        find k nearest neighbor(s) of q
        :param tree:  vp-tree
        :param q: a query point
        :param k: number of nearest neighbors
        """

        # buffer for nearest neightbors
        neighbors = PriorityQueue(k)
        #neighbors = []


        # list of nodes ot visit
        visit_stack = deque([self])

        # distance of n-nearest neighbors so far
        tau = np.inf

        while len(visit_stack) > 0:
            node = visit_stack.popleft()
            if node is None:
                continue

            d = self.dist_fn(q, node.vp)
            if d < tau:
                neighbors.push(d, node.vp)
                ##Reset tau if the queue is full
                if len(neighbors.queue) >= k : tau, _ = neighbors.queue[-1]


            if (node.left is None) and (node.right is None):
                continue

            #if d < node.mu:
            #    if d < node.mu + tau:
            #        visit_stack.append(node.left)
            #    if d >= node.mu - tau:
            #        visit_stack.append(node.right)
            #else:
            #    if d >= node.mu - tau:
            #        visit_stack.append(node.right)
            #    if d < node.mu + tau:
            #        visit_stack.append(node.left)

            #optimized nested if-statements
            if d < node.mu:
                visit_stack.append(node.left)
                if d >= node.mu - tau:
                    visit_stack.append(node.right)
            else:
                visit_stack.append(node.right)
                if d < node.mu + tau:
                    visit_stack.append(node.left)



        return neighbors.queue


    def get_all_in_range(self, q, tau):
        """
        find all points within a given radius of point q
        :param tree: vp-tree
        :param q: a query point
        :param tau: the maximum distance from point q
        """

        # buffer for nearest neightbors
        neighbors = []

        # list of nodes ot visit
        visit_stack = deque([self])

        while len(visit_stack) > 0:
            node = visit_stack.popleft()
            if node is None:
                continue

            d = self.dist_fn(q, node.vp)**(1./2)
            if d < tau:
                neighbors.append((d, node.vp))

            if node.is_leaf():
                continue

            if d < node.mu:
                if d < node.mu + tau:
                    visit_stack.append(node.left)
                if d >= node.mu - tau:
                    visit_stack.append(node.right)
            else:
                if d >= node.mu - tau:
                    visit_stack.append(node.right)
                if d < node.mu + tau:
                    visit_stack.append(node.left)
        return neighbors


class PriorityQueue(object):
    def __init__(self, size=None):
        self.queue = []
        self.size = size

    def push(self, priority, item):
        self.queue.append((priority, item))
        self.queue.sort()
        if self.size is not None and len(self.queue) > self.size:
            self.queue.pop()


### Distance functions2
#@profile
def quick_median(lst):
    lst = sorted(lst)
    if len(lst) < 1:
            return None
    if len(lst) %2 == 1:
            return lst[int((len(lst)+1)/2)-1]
    else:
            return float(sum(lst[int(len(lst)/2)-1:int(len(lst)/2)+1]))/2.0

#@profile
def l2(p1, p2):
    #a = p1.x
    #b = p2.x
    #return np.sqrt(np.sum(np.abs(p1.x - p2.x)**2))
    return (np.sum(np.abs(p1.x - p2.x)**2)) #do not apply square root yet, to speed up execution





# TODO: make this user selectable: {euclidean, correlation, manhattan, etc}
def distance(a,b):
    """
    Calculates the euclidean distance between the two genetic vectors
    """
    global comparison_count
    comparison_count +=1
    return np.linalg.norm(a[1] - b[1])


def save_tree(tree, filename='vptree.p'):
    """
    Saves the vp-tree to filename using pickle.
    """

    with open(filename, 'wb') as f:
        return pickle.dump(tree, f)


def load_tree(filename):
    """
    Returns the vp-tree stored in filename
    """

    with open(filename, 'rb') as f:
        return pickle.load(f)



def build(args):
    """
    Build the vp-tree and save it to a file.
    """

    # Define data locations in HDF5 file
    dataNode = '/Data'
    sampleNode = '/Sample'
    featureNode = '/Feature'

    # load data from HDF file
    h5File = h5py.File(args.hdf5filename, 'r')
    ds = h5File[dataNode][...]
    try:
        (nSample, nFeature) = ds.shape
    except ValueError:
        logging.error("Not enough samples in data set to build a tree.")
        raise RuntimeError("Not enough samples in data set to build a tree.")

    # TODO: load sample names and feature names as well
    if sampleNode in h5File.keys():
        sample = h5File[sampleNode][...]
    else:
        #sample = [int(uuid.uuid4()) for i in range(nSample)]
        sample = [i+1 for i in range(nSample)]

    if featureNode in h5File.keys():
        feature = h5File[featureNode][...]
    else:
        feature = range(nFeature)

    h5File.close()


    ds = ds.T
    dataTuple = [(sample[i], ds[i][:]) for i in range(nSample)]

    logging.info("Building tree...")
    points = [NDPoint(d,s) for s,d in zip(sample, ds.T)]
    tree = VPTree(points)

    logging.info("Saving tree to %s" % args.outputFile)
    save_tree(tree, args.outputFile)

def search(args):
    """
    Search the vp-tree for a query and print out the nearest neighbors.
    """

    # Load the tree from pickle file
    tree = load_tree(args.treeFilename)

    # load query from args.queryFilename
    f = h5py.File(args.queryFilename, 'r')
    query_data = f['Data'][...]
    # TODO: load sample names and feature names as well
    f.close()


    query = NDPoint(query_data,'Query')

    neighbors = tree.get_nearest_neighbors(query, args.K)
    for hit_distance, hit_point in neighbors:
        print(hit_point.name.decode("utf-8"), np.sqrt(hit_distance))

    # numresults = args.K
    # for result in tree.find(query):
    #     print(str(result[0][0]) + ' ' + str(comparison_count))
    #     numresults -=1
    #     if not numresults: break

def transformPCA(hdf5filename, n_components=10, outputfilename=None):
    """
    Transforms the data in the hdf5file and performs PCA. Writes data to a new hdf5 file
    """

    f = h5py.File(hdf5filename, 'r')
    GEXP = f['Expression'].value
    Sample_IDs = f['Samples'].value
    f.close()

    Samples = [GEXP[i,:] for i in range(len(Sample_IDs))]

    pca = PCA(n_components)

    pca_Samples =  pca.fit_transform(Samples)
    expression_pc = np.zeros(shape=(len(Samples), n_components))

    for idx in range(len(Samples)): expression_pc[idx, :] = pca_Samples[idx]

    out = h5py.File("pca_%s" % hdf5filename if not outputfilename else outputfilename, 'w')
    out.create_dataset('Samples', data=Sample_IDs)
    out.create_dataset('Components', data=expression_pc)
    out.close()

if __name__ == '__main__':

    import argparse

    # Populate our options, -h/--help is already there.
    description = """
                    gemini is a genomic data search engine.
                  """

    # create the top-level parser
    argp = argparse.ArgumentParser(prog='gemini', description=description)
    argp.add_argument('--version', action='version', version='%(prog)s 1.0')
    argp.add_argument('-v', '--verbose', dest='verbose', action='count', default=0,
                help="increase verbosity (specify multiple times for more)")

    subparsers = argp.add_subparsers(help='sub-command help')

    # create subparser for building the vp-tree
    argpBuild = subparsers.add_parser('build',
                        help='build a vp-tree from a HDF5 file containing data.')
    argpBuild.add_argument('hdf5filename',
                        help='name of HDF% file containing features x samples dataset in "Data"')
    argpBuild.add_argument('-o', dest='outputFile',
                default='vptree.p',
                help='file containing vp-tree')
    argpBuild.set_defaults(func=build)

    # create subparser to search the vp-tree
    argpSearch = subparsers.add_parser('search',
                        help='search for K neasest neighbors in a data set')
    argpSearch.add_argument('queryFilename', help='name of HDF5 file containing query vector in "Query"')
    argpSearch.add_argument('treeFilename', help='name of pickle file containing vp-tree')
    argpSearch.add_argument('--K', default=5, type=int, help='number of nearest neighbor matches to return')
    argpSearch.set_defaults(func=search)

    args = argp.parse_args()

    ## TODO check what came in on the command line and call optp.error("Useful message") to exit if all is not well
    log_level = logging.WARNING  # default
    if args.verbose == 1:
        log_level = logging.INFO
    elif args.verbose >= 2:
        log_level = logging.DEBUG

    # Set up basic configuration, out to stderr with a reasonable format
    logging.basicConfig(level=log_level,
                        format='%(levelname)s:%(module)s:%(message)s')

    # Do actual work here
    args.func(args)

